@extends('layouts.master')
@section('content')   
  <div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Update</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="cast/Edit">Back</a>
        </div>
    </div>
  </div>


<form action="{{ url('cast/update', $cast->id) }}" method="post">
@csrf
    {{-- @method('put') --}}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="from-grup">
                <strong>update</strong>
                 <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}"><br><br>
            <input type="text" class="form-control" name="nama" value="{{ $cast->nama }}"><br><br>

                    <label for="umur">Umur:</label><br>
                    <input type="text" class="form-control" name="umur" value="{{ $cast->umur }}"><br><br>

                    <label for="bio">Bio:</label><br>
                    <textarea name="bio" class="form-control" rows="5" cols="15">{{ $cast->bio }}</textarea><br><br>

                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
@endsection