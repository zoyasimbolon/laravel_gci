@extends('layouts.master')
@section('content')
<section class="content">
    <div class="card card-primary">

        <div class="card-header">
            <h3 class="card-title">Form Add</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->

        <form role="form" action="/cast" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Nama">
                </div>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Enter Umur">
                </div>
                @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" placeholder="Enter Bio">
                </div>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
    </div>
    </form>
</section>

@endsection