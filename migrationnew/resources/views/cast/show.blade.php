@extends('layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3>Detail Cast</h3>
        </div>
        <div class="card-body">
            <p>Nama: {{ $cast->nama }}</p>
            <p>Umur: {{ $cast->umur }}</p>
            <p>Bio: {{ $cast->bio }}</p>
        </div>
    </div>
@endsection
