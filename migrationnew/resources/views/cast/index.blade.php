@extends('layouts.master')
@section('content')
<br>
    <div class="new" >
        <a href="/cast/create" class="btn btn-primary">Add New +</a>
    </div>

    <div class="body">
        @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
        @endif
    </div>

    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <td style="text-align: center; font-weight:bold">No</td>
            <td style="text-align: center; font-weight:bold">Nama</td>
            <td style="text-align: center; font-weight:bold">Umur</td>
            <td style="text-align: center; font-weight:bold">Bio</td>
            <td style="text-align: center; font-weight:bold">Aksi</td>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
            <tr>
                <td style="text-align: center;">{{$key+1}}</td>
                <td style="text-align: center;">{{$value->nama}}</td>
                <td style="text-align: center;">{{$value->umur}}</td>
                <td style="text-align: center;">{{$value->bio}}</td>
                <td>
                    <div class="d-flex justify-content-center">
                        <a href="/cast/{{$value->id}}" class="btn btn-info m-2">Show</a>
                        <a href="{{ url('cast/edit/'.$value->id) }}" class="btn btn-primary m-2">Edit</a>
                        <form action="/cast/{{$value->id}}" method="POST">
                        <form action="{{ route('cast.destroy', $value->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-1 m-2">Delete</button>
                        </form>

                    </div>

                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Data</td>
            </tr>
            @endforelse
        </tbody>

    </table>
    </div>

@endsection