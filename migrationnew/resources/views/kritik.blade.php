@extends('layouts.master')
@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <td style="text-align: center; font-weight:bold">No</td>
        <td style="text-align: center; font-weight:bold">Content</td>
        <td style="text-align: center; font-weight:bold">Point</td>

    </thead>
    <tbody>
        @forelse ($kritik as $key => $value)
        <tr>
            <td style="text-align: center;">{{$key+1}}</td>
            <td style="text-align: center;">{{$value->content}}</td>
            <td style="text-align: center;">{{$value->point}}</td>

        </tr>
        @empty
        <tr>
            <td colspan="3" align="center">No Data</td>
        </tr>
        @endforelse
    </tbody>

</table>
@endsection