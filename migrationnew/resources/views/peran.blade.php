@extends('layouts.master')
@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <td style="text-align: center; font-weight:bold">No</td>
        <td style="text-align: center; font-weight:bold">Nama</td>

    </thead>
    <tbody>
        @forelse ($peran as $key => $value)
        <tr>
            <td style="text-align: center;">{{$key+1}}</td>
            <td style="text-align: center;">{{$value->nama}}</td>

        </tr>
        @empty
        <tr>
            <td colspan="2" align="center">No Data</td>
        </tr>
        @endforelse
    </tbody>

</table>
@endsection