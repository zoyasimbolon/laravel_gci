@extends('layouts.master')
@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <td style="text-align: center; font-weight:bold">No</td>
        <td style="text-align: center; font-weight:bold">Judul</td>
        <td style="text-align: center; font-weight:bold">Ringkasan</td>
        <td style="text-align: center; font-weight:bold">Tahun</td>
        <td style="text-align: center; font-weight:bold">Poster</td>
    </thead>
    <tbody>
        @forelse ($film as $key => $value)
        <tr>
            <td style="text-align: center;">{{$key+1}}</td>
            <td style="text-align: center;">{{$value->judul}}</td>
            <td style="text-align: center;">{{$value->ringkasan}}</td>
            <td style="text-align: center;">{{$value->tahun}}</td>
            <td style="text-align: center;">{{$value->poster}}</td>
        </tr>
        @empty
        <tr>
            <td colspan="5" align="center">No Data</td>
        </tr>
        @endforelse
    </tbody>

</table>
@endsection