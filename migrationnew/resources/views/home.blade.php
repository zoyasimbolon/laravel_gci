@extends('layouts.master')
@section('content')
 
</head>
<style>
     body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
            text-align: center; /* Center-align all content */
        }
        ul, ol {
            text-align: left;
            display: inline-block;
        }

        li {
            margin-top: 10px;
        }
</style>
<body>
    <h1>Garuda Cyber Institute</h1>
    <h3>Jadilah Programer Handal Bersama GC-INS</h3>
    <p>Grow Together With Garuda Cyber Institute</p>
    <h3>Syarat dan Ketentuan</h3>
    <ul>
        <li>Tamatan SMA/SMK</li>
        <li>Tamatan Perguruan Tinggi</li>
        <li>Pekerja IT</li>
        <li>Freelancer</li>
    </ul>
    <h3>Cara Bergabung</h3>
    <ol>
        <li>Kunjungi website GC-INS</li>
        <li>Register</li>
        <li>Lakukan pembayaran</li>
    </ol>
    
</body>
</html>
@endsection
