@extends('layouts.master')
@section('content')
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <td style="text-align: center; font-weight:bold">No</td>
        <td style="text-align: center; font-weight:bold">Nama</td>
        <td style="text-align: center; font-weight:bold">Email</td>
        <td style="text-align: center; font-weight:bold">Password</td>
    </thead>
    <tbody>
        @forelse ($user as $key => $value)
        <tr>
            <td style="text-align: center;">{{$key+1}}</td>
            <td style="text-align: center;">{{$value->nama}}</td>
            <td style="text-align: center;">{{$value->email}}</td>
            <td style="text-align: center;">{{$value->password}}</td>

            
        </tr>
        @empty
        <tr>
            <td colspan="4" align="center"></td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection