@extends('layouts.master')
@section('content')
<section> 
    <div class="new">
        <a href="/cast" class="btn btn-primary">Add New +</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
        <td style="text-align: center; font-weight:bold">No</td>
        <td style="text-align: center; font-weight:bold">Nama</td>
        <td style="text-align: center; font-weight:bold">Umur</td>
        <td style="text-align: center; font-weight:bold">Bio</td>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
        <tr>
            <td style="text-align: center;">{{$key+1}}</td>
            <td style="text-align: center;">{{$value->nama}}</td>
            <td style="text-align: center;">{{$value->umur}}</td>
            <td style="text-align: center;">{{$value->bio}}</td>
        </tr>
        @empty
        <tr>
            <td colspan="4" align="center">No Data</td>
        </tr>
        @endforelse
    </tbody>

</table>
</section>
@endsection