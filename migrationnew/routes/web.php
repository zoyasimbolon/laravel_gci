<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\KritikController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PeranController;

use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/', [DashboardController::class, 'index']);
Route::get('/dasboard', [DashboardController::class, 'index']);
Route::get('/user', [UserController::class, 'index']);
Route::get('/profile', [ProfileController::class, 'index']);
Route::get('/film', [FilmController::class, 'index']);
Route::get('/cast', [CastController::class, 'index']);
Route::get('/kritik', [KritikController::class, 'index']);
Route::get('/genre', [GenreController::class, 'index']);
Route::get('/peran', [PeranController::class, 'index']);
Route::get('/home', [HomeController::class, 'index']);

//untuk submit
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

//untuk delete
Route::delete('/cast/{id}', [CastController::class, 'destroy'])->name('cast.destroy');

//show
Route::get('/cast/{id}', [CastController::class, 'show']);

//edit
Route::get('cast/edit/{id}', [CastController::class, 'edit']);
Route::post('/cast/update/{id}', [CastController::class, 'update']);
