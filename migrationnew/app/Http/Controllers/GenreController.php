<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function index()
    {
        $genre = DB::table('genre')->get();
        return view('genre', compact('genre'));
    }
}
