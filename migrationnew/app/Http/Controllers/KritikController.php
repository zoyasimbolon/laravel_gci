<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KritikController extends Controller
{
    public function index()
    {
        $kritik = DB::table('kritik')->get();
        return view('kritik', compact('kritik'));
    }
}
