<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeranController extends Controller
{
    public function index()
    {
        $peran = DB::table('peran')->get();
        return view('peran', compact('peran'));
    }
}
