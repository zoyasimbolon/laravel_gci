<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <style> 
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
        }
        
        h2, h3 {
            text-align: center;
        }
        
        form {
            max-width: 400px;
            margin: 0 auto;
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }
        
        label {
            display: block;
            margin-top: 10px;
        }
        
        input[type="text"],
        select,
        textarea {
            width: 100%;
            padding: 10px;
            margin: 5px 0;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        
        input[type="radio"] {
            margin-right: 5px;
        }
        
        button {
            background-color: #0074b7;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 3px;
            cursor: pointer;
        }
        
        button:hover {
            background-color: #00568b;
        }
    </style>
</head>   
<body>
    <h2>Buat Account Baru</h2>
    <h3>Sign up Form</h3>
    <form method="POST" action="{{ route('register') }}">
        @csrf <!-- Laravel CSRF Token -->
        <form action="welcome.html">
        <label>First Name:</label><br>
        <input type="text" name="firstName"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lastName"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender" value="L">Male<br>
        <input type="radio" name="gender" value="F">Female<br>
        <input type="radio" name="gender" value="O">Other<br><br>
    <label>Nationaly:</label><br><br>

    <select id="country" name="country">
            <option value="Indonesia">Indonesia</option>
            <option value="Japan">Japan</option>
        </select><br><br>

        <label>Language spoken:</label><br>
        <input type="radio" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="radio" name="language" value="English">English<br>
        <input type="radio" name="language" value="Other">Other<br><br>

        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea>
        <br><br>

        <button type="submit">Sign up</button>
    </form>

    
    </body>
</html>