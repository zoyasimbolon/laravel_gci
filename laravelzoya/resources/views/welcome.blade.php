<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perintah Pengguna</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
        }
        
        h1 {
            text-align: center;
            margin-top: 50px;
        }
        
        h3 {
            text-align: center;
            margin-top: 20px;
        }

        #perintah {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        #hasilPerintah {
            text-align: center;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <h1>SELAMAT DATANG {{ $firstName }} {{ $lastName }}</h1>
    <h3>Terima kasih Telah Bergabung Bersama Garuda Cyber Institute</h3>
</body>
</html>