<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTablesController;
use App\Http\Controllers\DasboardController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/table',[TableController::class,'index'])->name('table');
route::get('/data-tables',[DataTablesController::class,'index'])->name('data-tables');
route::get('/',function() {
    return view('layout.master');
});
route::get('/dashboard',[DasboardController::class,'index'])->name('dashboard');
route::get('/home',[HomeController::class,'index'])->name('home');